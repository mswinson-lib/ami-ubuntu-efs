.PHONY: build style


build:
	packer build -var-file=variables.json template.json

style:
	packer validate -var-file=variables.json template.json


